//
//  MannualInvoiceView.m
//  Roadyo
//
//  Created by Rahul Sharma on 12/07/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import "MannualInvoiceView.h"


@implementation MannualInvoiceView
static NSArray *typeFareName;
static NSArray *typeFareAmount;

-(id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"mannualInvoiceScreen" owner:self options:nil] firstObject];
    return self;
}
-(void)onWindow:(UIWindow *)onWindow pickAddress:(NSString*)Pick dropAddress:(NSString*)drop commingFromMannual:(BOOL)mannual
{
    _isPopUpOPen=YES;
    self.currentWindow = onWindow;
    [self addShadowToView:self.contentView];
    self.frame = onWindow.frame;
    [onWindow addSubview:self];
    
    typeFareName = [[NSArray alloc]init];
    typeFareAmount = [[NSArray alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newBookingVCDismissed)
                                                 name:@"removePopUp" object:nil];
    
    _pickUpLocationLbl.text = _pickupAddressString;
    _dropOffLocationLbl.text = _dropOffAddressString;
    _totalAmtLbl.text =[NSString stringWithFormat:@"%@ %@", [Helper getCurrencyUnit],_amountString];
    //[NSString stringWithFormat:@"%@ %.02f", [Helper getCurrencyUnit],[_amountString doubleValue]+[_airportFeeString doubleValue]];
    _basefeeAmtLbl.text = [NSString stringWithFormat:@"%@ %@", [Helper getCurrencyUnit],_distanceFeeString];
    _distanceLbl.text = _distanceString;
    _durationLbl.text = _durationString;
    _airportFeeLbl.text =[NSString stringWithFormat:@"%@ %@", [Helper getCurrencyUnit],_airportFeeString];
    _timeFeeLbl.text = [NSString stringWithFormat:@"%@ %@", [Helper getCurrencyUnit],_timeFeeString];
    _meterAmtLbl.text = [NSString stringWithFormat:@"%@ %@", [Helper getCurrencyUnit],_baseFeeString];
    _tipLbl.text = [NSString stringWithFormat:@"%@ %@", [Helper getCurrencyUnit],_TipLblString];
    _bidLbl.text = [NSString stringWithFormat:@"BID: %@",_bidString];
    if (mannual)
    {
        [self.doneBtnClick setTitle:NSLocalizedString(@"DONE", @"DONE") forState:UIControlStateNormal];
    }
    else
    {
        [self.doneBtnClick setTitle:NSLocalizedString(@"RATE YOUR COUSTMER", @"RATE YOUR COUSTMER") forState:UIControlStateNormal];
    }
    
    
    if(_discountString == nil)
        
        _discountString = @"10";
    
    typeFareName = @[@"Base Fare",@"Distance Fare",@"Time Fare",@"Airport Fare",@"Tip",@"Total"];
    typeFareAmount = @[_baseFeeString,_distanceFeeString,_timeFeeString,_airportFeeString, _TipLblString,_amountString];
    //_discountString,subTotal
    //,@"Discount",@"Subtotal",
//    [_fareTableView setHidden:YES];
    _fareTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self hideItems];
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished)
     {
     }];
}

-(void)newBookingVCDismissed
{
    if (_isPopUpOPen)
    {
        [self doneButtonClickAction:nil];
    }
}
- (void) addShadowToView:(UIView *)view
{
    // border radius
    [view.layer setCornerRadius:10.0f];
    
    // border
    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [view.layer setBorderWidth:1.0f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.5];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(1.0, 2.0)];
}
- (IBAction)doneButtonClickAction:(id)sender
{
    if ([self.doneBtnClick.currentTitle isEqualToString:@"DONE"] )
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(doneButtonSelectWithTag)])
        {
            _isPopUpOPen=NO;
            [_delegate doneButtonSelectWithTag];
        }
        [self hidePOPup];
    }
    else
    {
        self.contentView.hidden = YES;
//        RateYourCustomer * rateController = [RateYourCustomer sharedInstance];
//        [rateController showServicesPopUpOnWindow:self.currentWindow];
//        rateController.delegate = self;
    }
    
}
#pragma mark - RateYourCustomer Delegate method
- (void)rateDoneButtonSelectWithTag
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(doneButtonSelectWithTag)])
    {
        [_delegate doneButtonSelectWithTag];
        [self removeFromSuperview];
    }
}
-(void)hidePOPup
{
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.contentView.alpha = 0.3;
                     }
                     completion:^(BOOL finished)
    {
                         [self removeFromSuperview];
                     }];
}

#pragma mark Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [typeFareName count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"fareDetails";
    UITableViewCell *cell;
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    
    UILabel *typeFareNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 150, 21)];
    [typeFareNameLabel setTextColor: UIColorFromRGB(0x333333)];
//    typeFareNameLabel.font = [UIFont fontWithName:Robot_CondensedRegular size:13];
    typeFareNameLabel.text = typeFareName[indexPath.row];
    
    UILabel *typeFareAmountLabel = [[UILabel alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-120, 5, 100, 21)];
    [typeFareAmountLabel setTextColor: UIColorFromRGB(0x333333)];
//    typeFareNameLabel.font = [UIFont fontWithName:Robot_CondensedRegular size:15];
    typeFareAmountLabel.text = [NSString stringWithFormat:@"%@ %@", [Helper getCurrencyUnit],typeFareAmount[indexPath.row]];
    typeFareAmountLabel.textAlignment = NSTextAlignmentRight-20;
    [cell.contentView addSubview:typeFareNameLabel];
    [cell.contentView addSubview:typeFareAmountLabel];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 33;
}

-(void)hideItems
{
    [_totalAmtLbl setHidden:YES];
    //[NSString stringWithFormat:@"%@ %.02f", [Helper getCurrencyUnit],[_amountString doubleValue]+[_airportFeeString doubleValue]];
    [_basefeeAmtLbl setHidden:YES];
    [_distanceLbl setHidden:NO];
    [_durationLbl setHidden:NO];
    [_airportFeeLbl setHidden:YES];
   [ _timeFeeLbl setHidden:YES];
   [ _meterAmtLbl setHidden:YES];
    [_tipLbl setHidden:YES];
//    [_bidLbl setHidden:YES];
    
    [_distanceFeeLabel setHidden:YES];
    [_baseFee setHidden:YES];
    [_timeFeeLabel setHidden:YES];
    [_airportFeeLabel setHidden:YES];
    [_tiplabel setHidden:YES];
    [_totalFeeLabel setHidden:YES];

}


@end
