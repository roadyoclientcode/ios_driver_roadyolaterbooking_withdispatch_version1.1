//
//  ShareCouponViewController.m
//  Tutree Tutor
//
//  Created by Rahul Sharma on 1/15/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "ShareCouponViewController.h"
#import "AppConstants.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import  "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "SplashViewController.h"
#import "LocationTracker.h"
#import "User.h"

@interface ShareCouponViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,CustomNavigationBarDelegate,UserDelegate>
@property (strong, nonatomic) NSString *postText;
@property (strong, nonatomic) IBOutlet UILabel *shareText;

@end

@implementation ShareCouponViewController
@synthesize postText;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self getCapital];
    
       
   // NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    //NSString *couponCode = [ud stringForKey:KNSUMasterCouponCode];
    
    NSString *inviteDrivers = NSLocalizedString(@"Invite other drivers to signup with", @"Invite other drivers to signup with");
    NSString *loyaltyPoints = NSLocalizedString(@"and we will provide you with some loyalty points.", @"and we will provide you with some loyalty points.");
    
    _shareText.text = [NSString stringWithFormat:@"%@ %@ %@",inviteDrivers,APP_NAME,loyaltyPoints];
    NSString *spread = NSLocalizedString(@"Spread the",@"Spread the");
    
    postText = [NSString stringWithFormat:@"%@ %@ %@",spread,APP_NAME,APP_DRIVER_ITUNES_LINK];
    
    [self addCustomNavigationBar];
   // self.labelText.text = @"Share this code and earn $5";
    //self.labelCouponCode.text = [NSString stringWithFormat:@"%@", couponCode];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
  

  
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttonFB:(id)sender {
    
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [tweetSheet setInitialText:postText];
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }

}

- (IBAction)buttonTweet:(id)sender {
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:postText];
        [self presentViewController:tweetSheet animated:YES completion:nil];
//    }
}

- (IBAction)buttonMsg:(id)sender {
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *message = [[MFMessageComposeViewController alloc] init];
        message.messageComposeDelegate = self;
        [[message navigationBar] setTintColor:[UIColor blackColor]];
        message.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
//        [message setSubject:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Join", @"Join"),APP_NAME]];
        [message setBody:postText];
        [self presentViewController:message animated:YES completion:nil];
    }
}

- (IBAction)buttonEmail:(id)sender {
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        // mail.navigationItem.backBarButtonItem.style = UIBarButtonItemStyleBordered;
        [[mail navigationBar] setTintColor:[UIColor blackColor]];
        mail.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
                [mail setSubject:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Join", @"Join"),APP_NAME]];
                [mail setMessageBody:postText isHTML:NO];
        //[mail setToRecipients:@[@"info@servodo.com"]];
        
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
    

    
    
}

- (IBAction)buttonBack:(id)sender {
    
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];

    
    
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"You cancelled sending message");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Message failed");
            break;
        case MessageComposeResultSent:
            NSLog(@"Message sent");
            break;
            
        default:
            NSLog(@"An error occurred while composing this message");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) addCustomNavigationBar{
    
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"INVITE", @"INVITE")];
    [self.view addSubview:customNavigationBarView];
    
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


-(void)userDidLogoutSucessfully:(BOOL)sucess {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
}
-(void)userDidFailedToLogout:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
}

-(void)accountDeactivated {
    
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}




@end
