//
//  CheckBookingStatus.m
//  Roadyo
//
//  Created by Surender Rathore on 27/06/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "CheckBookingStatus.h"
#import "XDKAirMenuController.h"
#import "SplashViewController.h"

@implementation CheckBookingStatus
@synthesize callblock;
static CheckBookingStatus  *bookingStatus;

+ (id)sharedInstance
{
    if (!bookingStatus)
    {
        bookingStatus  = [[self alloc] init];
    }
    return bookingStatus;
}

-(void)checkOngoingAppointmentStatus:(NSString *)appDate
{
    PMDReachabilityWrapper * reachbility = [PMDReachabilityWrapper sharedInstance];
    if ([reachbility isNetworkAvailable])
    {
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSString *appointmntDate; //= appDate;
    if (appDate == nil)
    {
        appointmntDate = @"";
    }
    
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":flStrForStr(sessionToken),
                             @"ent_dev_id":deviceID,
                             @"ent_user_type":@"1",
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             };
    
    NSLog(@"kSMGetAppointmentDetial %@",params);
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getApptStatus"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   NSLog(@"getattpstatus response>>>>>>>>>>>>>>>>>>>>%@",response);
                                   
                                   if (success)
                                   {
                                       [self parsepollingResponse:response];
                                   }
                               }];
    }
}

-(void)parsepollingResponse:(NSDictionary *)responseDict
{
    if (responseDict == nil)
    {
        if(self.callblock)
            self.callblock(0,responseDict);
        return;
    }
    else if ([responseDict objectForKey:@"error"])
    {
        if(self.callblock)
            self.callblock(0,responseDict);
    }
    else
    {
        if ([[responseDict objectForKey:@"errFlag"] integerValue] == 0)
        {
            int status = 0;
            NSDictionary *data = [responseDict[@"data"][0] mutableCopy];
            if (data)
            {
                status = [data[@"status"] integerValue];
            }
            if ([[responseDict allKeys] containsObject:@"data"])
            {
                responseDict = [responseDict[@"data"][0] mutableCopy];
            }
            self.callblock(status,responseDict);
        }
        else
        {
            if(self.callblock)
                self.callblock(0,responseDict);
        }
    }
}
-(void)userDidLogoutSucessfully:(BOOL)sucess{
    
}


@end
