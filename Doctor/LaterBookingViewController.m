//
//  3BookingViewController.m
//  Shiply
//
//  Created by Rathore on 01/10/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "LaterBookingViewController.h"
#import "LaterBookingCell.h"
#import "UIImageView+WebCache.h"
#import "OnBookingViewController.h"
#import "BookingNotificationView.h"
#import "UpdateBookingStatus.h"
#import "NewCalenderCell.h"
@interface LaterBookingViewController () <UIActionSheetDelegate>
@property(nonatomic,strong)NSMutableArray *laterBookings;
@property(nonatomic,weak)IBOutlet UITableView *laterBookingTableView;
@property(nonatomic,assign)int selectedIndex;
@property(nonatomic,assign)NSInteger bookingStatus;
@property(nonatomic, strong) NSDictionary *dictionary;
@end

@implementation LaterBookingViewController
@synthesize dictionary;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.title = NSLocalizedString(@"SCHEDULES", @"SCHEDULES");
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self sendRequestForLaterBooking];
}
-(void)viewDidAppear:(BOOL)animated{
    BookingNotificationView *notification = [[BookingNotificationView alloc] init];
    [notification updateBookingInformation:nil bookingNotificaiton:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_laterBookings count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section     //present in data souurce protocol
{
    return [_laterBookings[section][@"appt"] count];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 250;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return _laterBookings[section][@"date"];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   static NSString *CellIdentifier = @"newCalenderCell";   //must be indentical
   NewCalenderCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil)
    {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"calendarNewCell" owner:self options:nil];
        cell = [objects objectAtIndex:0];
    }
   // LaterBookingCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
   
    NSDictionary *dictionary1 = _laterBookings[indexPath.section][@"appt"][indexPath.row];
//    cell.labelAddressPickup.text = dictionary[@"addrLine1"];
//    cell.labelName.text = dictionary[@"fname"];
//    cell.labelAddressDrop.text = dictionary[@"dropLine1"];
//    cell.labelBookingId.text = dictionary[@"bid"];
//    
//    NSDateFormatter *df = [self serverformatter];
//    NSDate *pickupTime = [df dateFromString:dictionary[@"apntDt"]];
//    df = [self formatter];
//    NSString *time = [df stringFromDate:pickupTime];
//    cell.labelDate.text = time;
//
//    [cell.buttonPhoneNuber setTitle:dictionary[@"mobile"] forState:UIControlStateNormal];
//    
//    cell.labelBookingStatus.text = [self getStatusString:[dictionary[@"statCode"] integerValue]];
    
    
    [cell.bookingID setText:dictionary1[@"bid"]];
    [cell.passengerName setText:dictionary1[@"fname"]];
    // [cell.bookingTime setText:[event appDate]];
    [cell.bookingTime setText:dictionary1[@"apntTime"]];
    NSDateFormatter *formatter = [self serverformatter];
    NSDate *date = [formatter dateFromString:dictionary1[@"apntDt"]];
    
   //Local notifications prior 1 hour before booking.
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeInterval:-60*60 sinceDate:date];
    localNotification.timeZone = [NSTimeZone systemTimeZone];
    localNotification.alertBody = [NSString stringWithFormat:@"You've a pending ride at %@",dictionary1[@"apntTime"]];
    localNotification.alertAction = @"View";
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    if ([localNotification.fireDate compare:[NSDate date]] == NSOrderedDescending) {
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }

    
    
    [cell.pickupLocation setText:dictionary1[@"addrLine1"]];
    [cell.dropoffLocation setText:dictionary1[@"dropLine1"]];
    [cell.totalAmount setHidden:YES];
    [cell.totalDistance setHidden:YES];
    [cell.totalJourneyTime setHidden:YES];
    

//    cell.buttonCancel.tag = 100 + indexPath.row;
//    cell.buttonOnTheWay.tag = 500 + indexPath.row;
//    cell.buttonPhoneNuber.tag = 1000 + indexPath.row;
//    
//    [cell.buttonPhoneNuber addTarget:self action:@selector(call:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.buttonOnTheWay addTarget:self action:@selector(buttonOnTheWayClicked:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.buttonCancel addTarget:self action:@selector(buttonCancelClicked:) forControlEvents:UIControlEventTouchUpInside];
   
    
    return cell;
}

/*
 specified row is now selected.
 parameter tableview == A table-view object informing the delegate about the new row selection
 parameter indexaPath == An index path locating a row in tableView.
 */
-(void)tableView : (UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Message" message:@"You want to proceed with this job?" preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *yes = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        dictionary = _laterBookings[indexPath.section][@"appt"][indexPath.row];
        NSMutableDictionary *mdict = [[NSMutableDictionary alloc] init];
        [mdict setObject:dictionary[@"email"] forKey:@"e"];
        [mdict setObject:dictionary[@"apntDt"] forKey:@"dt"];
        [mdict setObject:dictionary[@"bid"] forKey:@"rid"];
        
        
        [[NSUserDefaults standardUserDefaults] setObject:mdict forKey:@"PUSH"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        _bookingStatus = [dictionary[@"statCode"] integerValue];
    
        [self performSegueWithIdentifier:@"BookingDetail" sender:self];

  //  }];
    
//   UIAlertAction *no = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//       [controller dismissViewControllerAnimated:YES completion:nil];
//   }];
//    
//    [controller addAction:yes];
//    [controller addAction:no];
//    [self presentViewController:controller animated:YES completion:nil];

    
    //[self performSegueWithIdentifier:@"BookingDetail" sender:self];

    
}

-(void)sendRequestForLaterBooking{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString  *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSString *currentDate = [Helper getCurrentDateTime];
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:sessionToken forKey:@"ent_sess_token"];
    [params setObject:deviceID forKey:@"ent_dev_id"];
    [params setObject:currentDate forKey:@"ent_date_time"];
    
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getPendingAppts"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   
                                  
                                   [pi hideProgressIndicator];
                                   
                                   if (success) { //handle success response
                                       NSLog(@"response Later Booking : %@", response);
                                       
                                       if ([response[@"errFlag"] integerValue] == 0) { //success
                                           _laterBookings = [[NSMutableArray alloc] initWithArray:response[@"appointments"]];
                                            NSLog(@"Later Bookings: %@",_laterBookings);
                                           if ([_laterBookings count] > 0) {
                                               [self.viewNoBookings setHidden:YES];
                                           }else {
                                               [self.viewNoBookings setHidden:NO];
                                           }
                                           [[UIApplication sharedApplication] cancelAllLocalNotifications];
                                           [_laterBookingTableView reloadData];
                                       }else if ([response[@"errFlag"] integerValue] == 1) {
                                           [pi hideProgressIndicator];
                                           [self.viewNoBookings setHidden:NO];
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                           [_laterBookings removeAllObjects];
                                           [[UIApplication sharedApplication] cancelAllLocalNotifications];
                                           [_laterBookingTableView reloadData];
                                       }
                                       
                                   }
                                   else {
                                       
                                   }
                               }];
}


-(void)buttonCancelClicked:(UIButton*)sender{
    
    _selectedIndex = (int)(100 - sender.tag);
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Why are you canceling?", @"Why are you canceling?") delegate:self cancelButtonTitle:NSLocalizedString(@"No don't cancel booking", @"No don't cancel booking") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Do not charge client", @"Do not charge client"),NSLocalizedString(@"Client no-show", @"Client no-show"),NSLocalizedString(@"Client requested cancel", @"Client requested cancel"),NSLocalizedString(@"Wrong address shown", @"Wrong address shown"),NSLocalizedString(@"other", @"other") ,nil];

    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    if ([window.subviews containsObject:self.view]) {
        [actionSheet showInView:self.view];
    } else {
        [actionSheet showInView:window];
    }
}
-(void)buttonOnTheWayClicked:(UIButton*)sender{
    
    
    PMDReachabilityWrapper * reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable]) {
    
        NetworkHandler * handler = [NetworkHandler sharedInstance];
        
        
    NSDictionary *dictP = _laterBookings[(500-sender.tag)];
   
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   dictP[@"e"],kSMPRespondPassengerEmail,
                   dictP[@"dt"], kSMPRespondBookingDateTime,
                   constkNotificationTypeBookingOnTheWay,kSMPRespondResponse,
                   @"testing",kSMPRespondDocNotes,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    NSLog(@"MethodupdateApptStatus.....laterBooking%@",queryParams);
        [handler composeRequestWithMethod:MethodupdateApptStatus
                                  paramas:queryParams
                             onComplition:^(BOOL succeeded, NSDictionary *response) {
    
//    [restkit composeRequestForUpdateStatus:MethodupdateApptStatus
//                                   paramas:queryParams
//                              onComplition:^(BOOL success, NSDictionary *response){
//                                  
                                   if (succeeded) { //handle success response
                                      //[self updateStstusResponse:(NSArray*)response];
                                  }
                                  else{//error
                                      ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                      [pi hideProgressIndicator];
                                  }
                              }];
    }else{
        ProgressIndicator * pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
    
}
-(void)call:(id)sender{
    
}


-(void)sendRequestForCancelBookingWithReason:(CancelBookingReasons)reason{
    
    
   
    NSDictionary *passDetail = _laterBookings[_selectedIndex];
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable]) {
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showPIOnView:self.view withMessage:NSLocalizedString(@"cancelling..", @"cancelling..")];
        
        NSString *email = passDetail[@"email"];
        NSString *appointmentDate = passDetail[@"apptDt"];
        
        NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
        
        NSString *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
        
        NSString *currentDate = [Helper getCurrentDateTime];
        
        NSDictionary *params = @{@"ent_sess_token":sessionToken,
                                 @"ent_dev_id":deviceID,
                                 @"ent_pas_email":email,
                                 @"ent_appnt_dt":appointmentDate,
                                 @"ent_date_time":currentDate,
                                 @"ent_cancel_type":[NSNumber numberWithInt:reason],
                                 };
        
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:MethodCancelBooking
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       if (success) { //handle success response
                                           //[self cancelBookingResponse:response];
                                           if ([response[@"errFlag"] intValue] == 0) { //success
                                               
                                               
                                               [self.laterBookingTableView beginUpdates];
                                               NSIndexPath *ip = [NSIndexPath indexPathForRow:_selectedIndex inSection:0];
                                               [self.laterBookingTableView deleteRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationMiddle];
                                               [self.laterBookingTableView endUpdates];
                                               
                                               
                                               
                                               
                                           }
                                           else {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
                                           }
                                       }
                                       else {
                                           
                                           [pi hideProgressIndicator];
                                       }
                                   }];
    }
    else {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
}



#pragma mark - ActionSheet Delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.cancelButtonIndex != buttonIndex) {
        switch (buttonIndex) {
            case 0:  // do not charge client
            {
                [self sendRequestForCancelBookingWithReason:kCBRDoNotChargeClient];
                break;
            }
            case 1: // client no - show
            {
                [self sendRequestForCancelBookingWithReason:kCBRPassengerDoNotShow];
                break;
            }
            case 2: // client request to cancel
            {
                [self sendRequestForCancelBookingWithReason:kCBRDPassengerRequestedCancel];
                break;
            }
            case 3: // wrong address shown
            {
                [self sendRequestForCancelBookingWithReason:kCBRWrongAddressShown];
                break;
            }
            case 4: // other
            {
                [self sendRequestForCancelBookingWithReason:kCBOhterReasons];
                break;
            }
            default:
                break;
        }
    }
}

-(NSString*)getStatusString:(NSInteger)statusCode{
    
    switch (statusCode) {
        case kNotificationTypeBookingAccept1:
        {
            return NSLocalizedString(@"ACCEPTED", @"ACCEPTED");
            break;
        }
        case kNotificationTypeBookingOnTheWay:
        {
            return NSLocalizedString(@"ON THE WAY", @"ON THE WAY");
            break;
        }
        case kNotificationTypeBookingComplete:
        {
            return NSLocalizedString(@"COMPLETED", @"COMPLETED");
            break;
        }
        case kNotificationTypeBookingBeginTrip:
        {
            return NSLocalizedString(@"PICKED UP", @"PICKED UP");
            break;
        }
        case kNotificationTypeBookingArrived:
        {
            return NSLocalizedString(@"ARRIVED", @"ARRIVED");
            break;
        }
        case kNotificationTypeBookingReject1:
        {
            return NSLocalizedString(@"CANCELED", @"CANCELED");
            break;
        }
        default:
            break;
    }
    return @"";
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"BookingDetail"]) {
        OnBookingViewController *onBooking = (OnBookingViewController*)segue.destinationViewController;
        onBooking.bookingStatus = _bookingStatus;
        onBooking.LoadFrom = 1;
    }
}

- (NSDateFormatter *)serverformatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    });
    return formatter;
}
- (NSDateFormatter *)formatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM HH:mm";
    });
    return formatter;
}
-(void)sendRequestForUpdateStatus
{
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    [updateBookingStatus stopUpdatingStatus];
    NSString *email = dictionary[@"email"];
    NSString *appointmentDate = dictionary[@"apntDt"];
    NSDictionary *queryParams;
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                       [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                       email,kSMPRespondPassengerEmail,
                       appointmentDate, kSMPRespondBookingDateTime,
                       constkNotificationTypeBookingOnTheWay,kSMPRespondResponse,
                       @"testing",kSMPRespondDocNotes,
                       [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateApptStatus"
                                    paramas:queryParams
                               onComplition:^(BOOL success, NSDictionary *response)
    {
                                   NSLog(@"Response update status1: %@", response);
                                   ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
                                   //                                  [progressIndicator hideProgressIndicator];
                                   
                                   if ([response[@"errFlag"] integerValue]== 0) { //handle success response
                                       NSLog(@"Response update status: %@", response);
                                       [self performSegueWithIdentifier:@"BookingDetail" sender:self];
                                      // [self updateToPassengerForDriverState:kPubNubStartDoctorLocationStreamAction withIteration:-1];
                                   }
                                   else {
                                       
                                       [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"errMsg"]];
                                       //[pi hideProgressIndicator];
                                   }
                               }];
}

-(void)updateToPassengerForDriverState:(PubNubStreamAction)state withIteration:(int)iteration{
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    updateBookingStatus.driverState = state;
    updateBookingStatus.iterations = iteration;
    [updateBookingStatus updateToPassengerForDriverState];
    [updateBookingStatus startUpdatingStatus];
}
@end
