//
//  LaterBookingViewController.h
//  Shiply
//
//  Created by Rathore on 01/10/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaterBookingViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *viewNoBookings;
@end
